﻿DROP DATABASE IF EXISTS mensajes2024;
CREATE DATABASE mensajes2024;
USE mensajes2024;

CREATE TABLE mensajes(
id int AUTO_INCREMENT,
titulo varchar(100),
texto text,
fecha date,
PRIMARY KEY(id)
);

insert INTO mensajes (titulo, texto, fecha)
  VALUES ('clase 1', 'comenzando con Yii', '2024-05-03');
