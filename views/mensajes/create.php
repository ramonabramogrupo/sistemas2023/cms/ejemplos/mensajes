<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\models\Mensajes $model */

$this->title = 'Create Mensajes';
$this->params['breadcrumbs'][] = ['label' => 'Mensajes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mensajes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
